// Copyright 2023 Jan Mercl. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"os"
	"path/filepath"
	"time"
)

func main() {
	dir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}

	dir = filepath.Join(dir, "go", "telemetry")
	for range time.NewTicker(time.Minute).C {
		os.RemoveAll(dir)
	}
}
